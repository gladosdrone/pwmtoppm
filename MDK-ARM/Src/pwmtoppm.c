#include "pwmtoppm.h"

channel_data_type channel_data;

//initialize all channel_data_type variables
void  channel_data_init(){
    //Set all 9 input and output values to 0
    memset(channel_data.input, 0, 9);
    memset(channel_data.output, 2*TGAP, 9);

    channel_data.it_time = 0;

    //Set Error-flag to 1 since it will be cleared by software later on
    //Error-flag is cleared after all 8 Channels are successfully measured
    channel_data.error_flag = 1;

    //Set length of sync-pulse to max possible High-time of ppm-signal
    channel_data.sync_length = TSYNCBASE;

    //Set default values for CH9 which is not used
    channel_data.input[8] = TINMIN;
    channel_data.output[8] = TPULSE_MIN;
}

//Convert time of input pwm signals to the correct times of the output ppm signal
void channel_data_convert(){

    //Set length of sync-pulse again as it may have already been altered
    channel_data.sync_length = TSYNCBASE;

    //Loop through all channels
    for(int i=0; i<8; i++){

       //Convert input time to output time
       channel_data.output[i] = convert_number_range(channel_data.input[i], TINMIN, TINMAX, TPULSE_MIN + TGAP, TPULSE_MAX + TGAP);

       //Recalculate sync-pulse length
       channel_data.sync_length -= channel_data.output[i];
    }
		TIM1->CR1 |= 0x1;
}

//Convert measured input time in the range of inmin-inmax to the correct output time range of outmin-outmax
int convert_number_range(int input, int inmin, int inmax, int outmin, int outmax) {
    return  (((input - inmin) * (outmax - outmin)) / (inmax - inmin)) + outmin;
}

//Initializes TIM1 & TIM2
void timer_init(){
    //Sets the delay time of the One Pulse mode of TIM1 to TGAP
    //ARR set to placeholder value TGAP - will cause overflow until all channels are measured
    //Maybe change init of ARR to higher value? - e.g. ~8*2ms = 16ms since by then all channels have been active for at least one period
		TIM1->CR1 &= ~(0x1);
		TIM2->CR1 &= ~(0x1);	
	
		//Disable Lock
		TIM1->BDTR &= 0x0000;
		//Main Output Enable & OSSR = 1
		TIM1->BDTR |= (0x1<<15) | (0x1<<11); 
	
	
		TIM1->CR1 &= 0x0000;
		TIM1->DIER &= 0x0000;
		TIM1->DIER |= 0x1; //Update Interrupt enable
		TIM1->CCMR1 &= 0x0000; // CC2 Mode to Output
		TIM1->CCMR1 |= 0x7<<12; //PWM Mode 2
		TIM1->CCER &= 0x0000; // Polarity to active high
		TIM1->CCER |= 0x1<<4; // CC2 Output enable
		

    TIM1->CCR2 = TGAP;
    TIM1->ARR = TGAP;
		
		TIM2->DIER |= 0x1; //enable Update interrupt
//		TIM1->CCMR1 &= ~(0x7<<12);
//		TIM1->CCMR1 |= 0x7<<12;
//		TIM1->CCMR1 &= ~(0x3<<8);
//	
//		TIM1->DIER |= 0x1;
//		TIM1->CR1 &= ~(0x1<<1);
//		TIM1->CCER |= 0x1<<4;
//		TIM1->CCER &= ~(0x1<<5);

    //Start Timers TIM1 & TIM2
    TIM2->CR1 |= 1;
    TIM1->CR1 |= 1;
}
