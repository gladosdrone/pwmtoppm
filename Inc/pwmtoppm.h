#ifndef PWMTOPPM_PWMTOPPM_H
#define PWMTOPPM_PWMTOPPM_H

#include <string.h>
#include <stm32f1xx_hal.h>

// Output Timings
// ftick = 4MHz
// PPM Period = 22.42ms
// tsignal + tsync = PPM Period = const
//#define TTOLERANCE 20                  // 5us
#define TGAP 1200        // 300us ...Length of Low_Puls
#define TPULSE_MIN 3000  // 750us ...Min length of High-Pulse
#define TPULSE_MAX 6000   // 1.5ms ..Max length of High-Pulse
#define TSYNCMAX 50680    // 12.67ms ...Max possible length of sync-pulse
#define TSYNCBASE 89680-10*TGAP // 22.42ms - 10*TGAP = 19,42ms... Length without gaps

#define TNEXT TPULSE_MAX+TGAP
#define TMEAS_MAX TSYNCMAX+TGAP+TGAP


// Input Timings
#define TINMIN 1000     // 1ms ...Min length of High-State (PWM)
#define TINMAX 2000     // 2ms ...Max length of High-State (PWM)

typedef struct {
    //input and output array for CH1 - CH9
    //CH9 is not used and only implemented for PCM Mode
    uint16_t input[9];
    uint16_t output[9];

    //CNT-Value at last interrupt of TIM2 - used for pulse width computation
    uint16_t it_time;

    //Flag indicates if all pwm signals were measured
    uint8_t error_flag;

    //Length of sync-pulse at the end of the ppm signal
    uint16_t sync_length;
}channel_data_type;

extern channel_data_type channel_data;

void channel_data_init(void);
void channel_data_convert(void);
int convert_number_range(int input, int inmin, int inmax, int outmin, int outmax);
void timer_init(void);


#endif //PWMTOPPM_PWMTOPPM_H
